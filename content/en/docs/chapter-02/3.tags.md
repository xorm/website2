---
title: "Tag mapping"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-02"
weight: 9300
toc: true
---

### 2.3.Tag mapping

Using `names.Mapper` for all naming is the best choice. But if table or column is not in rule, we need new method to archive.

* If struct or pointer of struct has `TableName() string` method, the return value will be the struct's table name.

* `engine.Table()` can change the database table name for struct. The struct tag `xorm:"'column_name'"` can set column name for struct field. Use a pair of single quotes to prevent confusion for column's definition in struct tag. If there is no risk of confusion, ignore single quotes.
