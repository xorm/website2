---
date: "2019-12-28T16:56:00+08:00"
title: "Reverse"
weight: 10
toc: false
draft: false
menu: "sidebar"
goimport: "xorm.io/reverse git https://gitea.com/xorm/reverse"
gosource: "xorm.io/reverse https://gitea.com/xorm/reverse https://gitea.com/xorm/reverse/tree/master{/dir} https://gitea.com/xorm/reverse/blob/master{/dir}/{file}#L{line}"
---

# A flexsible and powerful command line tool to convert database to codes

This is the URL of the import path for [Reverse](https://gitea.com/xorm/reverse).
